# Andreani 2.3 Patches


### Descripción

Patches para reemplazar líneas de archivos del módulo DrubuNet Andreani 2.3 que no funcionan correctamente

### Instalación

```
composer require lyracons/andreani2.3-patches
```

O con la herramienta lyra tool:

```
lyra composer -- require lyracons/andreani2.3-patches
```

### Dependencias

Los patches requieren los módulos `andreani/module-andreani-magento-2.3` (el módulo que emparchan) y `cweagans/composer-patches`, que permite aplicar parches desde composer.
